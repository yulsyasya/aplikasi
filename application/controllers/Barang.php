<?php
defined('BASEPATH') OR exit('no direct script acess allowec');

class Barang extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('model');
	}
	function index(){
		$data = ['barang'=>$this->model->tampil('barang')];
		$this->load->view('v_tampil',$data);
	}
	function tambah(){
		$this->load->view('input');
	}
}