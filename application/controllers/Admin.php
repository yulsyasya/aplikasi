<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
		function __construct(){
		parent::__construct();		
		$this->load->model('model');
 		if (isset($_SESSION['status'])) {
 			if ($_SESSION['status'] == 'logout') {
 				redirect(base_url('admin'));
 			}
 		}
	}

	public function index()
	{
		if (isset($_SESSION['status']) && $_SESSION['status']=='login') {
			redirect('admin/menu');
		}
		$this->load->view('registrasi');
	}
	public function login()
	{
		if (isset($_SESSION['status']) && $_SESSION['status']=='login') {
			redirect('admin/menu');
		}
		$this->load->view('login');
	}
	public function menu()
	{
		$data = [
			'halaman' => 'registrasi'
		];

		$this->load->view('header', $data);
		$this->load->view('menu');
		$this->load->view('footer');
	}
	public function registrasi()
	{
		$this->load->view('registrasi');
	}
	public function input()
	{
		$data = [
			'halaman' => 'input'
		];

		$this->load->view('header', $data);
		$this->load->view('input');
		$this->load->view('footer');
	}

	function listbarang() {
		$pesan = '';
		if (isset($_GET['pesan'])) {
			$pesan = $_GET['pesan'];
		}
		$data = [
			'pesan' => $pesan,
			'halaman' => 'listbarang',
			'barang' => $this->model->tampil('barang'),
		];

		$this->load->view('header', $data);
		$this->load->view('listbarang');
		$this->load->view('footer');
	}

	function hapus() {
		$this->model->hapus('barang', ['id' => $_GET['id']]);
		redirect('admin/listbarang?pesan=Data berhasil dihapus.');
	}

	function logout(){
		$this->session->sess_destroy();
		$data_session = array(
			'status' => "logout",
		);
		$this->session->set_userdata($data_session);
		redirect(base_url('admin'));
	}


	public function registrasidata()
	{
		$name = $this->input->post('name');
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$repassword = $this->input->post('repassword');

		// validasi
		$this->form_validation->set_rules('name','Nama','required');
		$this->form_validation->set_rules('username','username','required');
		$this->form_validation->set_rules('password','password','required');
		$this->form_validation->set_rules('repassword','repassword','required');

		if($this->form_validation->run() != false){
			if($password == $repassword) {
				$data = array(
					'nama' => $name,
					'username' => $username,
					'pasword' => $password,
				);
				$this->db->insert('admin',$data);
				redirect('admin/login');

			}
			redirect('admin');
		} else {
			$this->load->view('registrasi');
		}
	}
	public function ceklogin() {
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$query = $this->db->get_where('admin', array('username' => $username,'pasword' => $password,))->result_array();
		if (count($query)>0) {
			$data_session = array(
				'nama' => $query['nama'],
				'username' => $query['username'],
				'status' => "login"
			);
			$this->session->set_userdata($data_session);

			redirect('admin/menu');

		} else {
			redirect('admin/login');
		}
	}
	function tambah_aksi(){
		$nama = $this->input->post('nama');
		$harga_barang = $this->input->post('harga_barang');
		$jumlah_barang = $this->input->post('jumlah_barang');
 
		$data = array(
			'nama' => $nama,
			'harga_barang' => $harga_barang,
			'jumlah_barang' => $jumlah_barang
			);
		$this->model->input_data($data,'barang');
		redirect('admin/listbarang?pesan=Data berhasil ditambah.');
	}
	
	function edit() {
		$data = [
			'halaman' => 'registrasi',
			'b' => $this->model->getSatu('barang', ['id' => $_GET['id']]),
		];
		$this->load->view('header', $data);
		$this->load->view('editBarang');
		$this->load->view('footer');
	}

	function editProses() {
		$data = [
			'nama' => $_POST['nama'],
			'harga_barang' => $_POST['harga_barang'],
			'jumlah_barang' => $_POST['jumlah_barang'],
		];
		$this->model->edit('barang', ['id' => $_POST['id']]);
		redirect(base_url('admin/listbarang?pesan=Data Berhasil diedit.'));
	}
}
