<?php

class Model extends CI_model{
	function tampil($tabel){
		return $this->db->get($tabel)->result();
	}
	function input_data($data,$table){
		$this->db->insert($table,$data);
	}
	function hapus($table, $kondisi) {
		$this->db->delete($table, $kondisi);
	}

	function getSatu($table, $kondisi) {
		return $this->db->get_where($table, $kondisi)->row();
	}

	function edit($table, $data) {
		$this->db->update($table, $data);
	}
}