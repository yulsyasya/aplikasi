<div class="container" style="margin-top: 50px">
	<?php if ($pesan != ''): ?>
		<div class="alert alert-primary"><?= $pesan ?></div>
	<?php endif ?>
	<table class="table table-hover">
		<tr>
			<th>No.</th>
			<th>Nama</th>
			<th>Harga</th>
			<th>Stok</th>
			<th>Aksi</th>
		</tr>
		<?php $no=1; foreach ($barang as $b): ?>
		<tr>
			<td><?= $no ?></td>
			<td><?= $b->nama ?></td>
			<td><?= $b->harga_barang ?></td>
			<td><?= $b->jumlah_barang ?></td>
			<td>
				<!-- hapus -->
				<a href="<?= base_url() ?>admin/hapus?id=<?= $b->id ?>" class="btn btn-danger"><i class="fas fa-trash"></i></a>
				<!-- edit -->
				<a href="<?= base_url() ?>admin/edit?id=<?= $b->id ?>" class="btn btn-warning"><i class="fas fa-edit"></i></a>
			</td>
			<?php $no++ ?>
		</tr>
		<?php endforeach ?>
	</table>
</div>