<!doctype html>
<html lang="en">

<head>
    <title>Hamburger Menu</title>
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/cssmenu.css">

    <!-- bootstrap -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>

    <!-- fa -->
    <script src="https://kit.fontawesome.com/b119fa7c50.js" crossorigin="anonymous"></script>
</head>

<body>
    <nav class="navbar">
        <a class="toggler-navbar">
            <div class="hamburger-menu">
                <span></span>
                <span></span>
                <span></span>
            </div>
        </a>
        <a class="logo" href="">
            MENU
        </a>
    </nav>
    <div class="sidebar">
        <ul>
            <li <?php if ($halaman == 'registrasi'): ?>
                class="active"
            <?php endif ?>><a href="http://localhost/aplikasi/admin/registrasi">Registrasi</a></li>
            <li><a href="http://localhost/aplikasi/admin/logout">Logout</a></li>
            <li <?php if ($halaman == 'input'): ?>
                class="active"
            <?php endif ?> ><a href="http://localhost/aplikasi/admin/input">Input</a></li>
            <li <?php if ($halaman == 'listbarang'): ?>
                class="active"
            <?php endif ?> ><a href="http://localhost/aplikasi/admin/listbarang">Daftar Barang</a></li>
             <li><a href="">Transaksi</a></li>
        </ul>
    </div>